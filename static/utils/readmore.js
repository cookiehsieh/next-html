/*

長長文字的 ...查看更多

*/

$(function () {
  $('.dynamic-height .read-more').on('click', function(e) {
    e.preventDefault();

    var $this = $(this);
    var $wrapper  = $this.parent();
    var $contents = $wrapper.find('p');

    if ($this.hasClass('active')) {
      $wrapper
        .animate({
          'height': $wrapper.data('maxHeight'),
        });

      $this.removeClass('active');
    } else {
      var totalHeight = 0;
      // measure how tall inside should be by adding together heights of all inside paragraphs (except read-more paragraph)
      $contents.each(function() {
        totalHeight += $(this).outerHeight(true);
      });

      totalHeight += $this.outerHeight(true);

      $wrapper
        .css({
          // Set height to prevent instant jumpdown when max height is removed
          'height': $wrapper.height(),
          'max-height': 9999,
        })
        .animate({
          'height': totalHeight,
        });

      $this.addClass('active');
    }
  });
});
