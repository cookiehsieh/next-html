import { Fragment } from 'react';

export default ({bottomScripts}) => {
  return (
    <Fragment>
      { bottomScripts.map((script, index) => <script key={index} src={script} />) }
    </Fragment>
  );
}
