import Layout from 'components/layout';
import Footer from 'components/footer';

const bottomScripts = [
  'static/js/bottom.js',
];

export default () => (
  <Layout title="StreetVoice" bottomScripts={bottomScripts}>
    <div class="test">TEST HELLO WORLD</div>
    <Footer />
  </Layout>
);
