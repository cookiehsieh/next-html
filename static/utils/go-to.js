/*

go to 某個 section

*/

$(function () {
  const elements = document.querySelectorAll('a.go-to');

  const options = {
    rootMargin: '0px 0px 20px 0px',
    threshold: 0,
  };
  const callback = (entries) => {
    entries.forEach(entry => {
      if (entry.isIntersectinㄌg) {
        entry.target.classList.add('active');
      } else {
        entry.target.classList.remove('active');
      }
    });
  }
  const observer = new IntersectionObserver(callback, options);

  function gotoSection(element) {
    element.addEventListener('click', (e) => {
      e.preventDefault();
      document.querySelector(element.hash).scrollIntoView({ behavior: 'smooth', block: 'end' });
    });
  }

  elements.forEach((element) => {
    observer.observe(element);
    gotoSection(element);
  });
});
