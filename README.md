## Installation

```
npm install
```

## Development

執行 `npm run dev`

瀏覽器開啟 http://localhost:3000

## Export

執行 `npm run export`

完成後輸出的檔案們會在 `build/` 裡

## Structure
```
├── build                   # Exported files
├── components              # Shared components
│   ├── layout.js           # Layout (container of head, body, scripts, ...)
│   ├── scripts.js          # Included js files
│   └── ...
├── docs
├── pages                   # JS files inside here will be compiled into [filename].html
├── static                  # Static files
│   ├── images
│   ├── js
│   ├── libs
│   └── scss
├── format.js               # Format html and copy static files to `builds/`
├── next.config.js
├── package.json
└── README.md
```

## Guide

**pages**

1. 這邊放的是頁面，檔名最後都會變成網址
2. 有幾個注意事項：

- 一開始都要先 import layout
- `import Xyz from 'components/xyz'` 的 Xxx 第一個字母一定要大寫（檔名的話通常第一個字會是小寫）
- 再 import 其他你需要的 components
- 把 html 用 `export default () => (<Layout>{ 真正的 html 放這兒 }</Layout>);` 包起來
- html 裡面如果是前面 import 來的東西，都要記得大寫，比如說 `<Footer />`
- 每個 html tag 都要 self colsing，比如說 `<Footer />` ， `<img src="..." />` ，`<textarea />`
- 如果需要用 inline css ，要用 `{{}}` 包起來，比如說 `<div style={{ color: 'red' }}>StreetVoice</div>`

最後大概會長這樣：

```
import Layout from 'components/layout';
import Footer from 'components/footer';

export default () => (
  <Layout title="StreetVoice">
    <div style={{ color: 'red' }}>StreetVoice</div>
    <a href="test">test</a>
    <img src="static/logo-sv.png" />
    <Footer />
  </Layout>
);
```

**components**

這邊放的就是共用的 components，比如說 footer、header 那類的

寫法和 pages 的差別在於 components 外面不需要用 `Layout` 包住，長這樣：

```
export default () => (
  <div>I AM Footer</div>
);
```

但如果你的 component 沒有用 html tag 包起來，這樣直接儲存的話會 error，比如說：

```
export default () => (
  <div>I AM Footerrrr</div>
  <a href="#">link</a>
);

```

這是因為不管怎樣每個檔案都必須輸出 **一個 element**，
單純用 div 包起來也是可以，只是就會長出不必要的 div，
所以這邊用 Fragment 包起來，像這樣：

```
import { Fragment } from 'react'; // 先 import

export default () => (
  <Fragment> // 再包
    <div>I AM Footerrrr</div>
    <a href="#">link</a>
  </Fragment> // closing tag
);
```

Done!
