/*

！！！！ page-loader 加了 id，比較合理！！！！

在大檔案們讀取完畢之後，才把 page-loader 拿掉

- 這隻 js 要放在 head 裡的 css 前面（如果 css 裡面沒有要 preload 的圖的話，就不需要放在 css 前面）
- 幫 page-loader 加 css transition: opacity 1s;

要 preload 的 image 請放在 images 裡
要 preload 的 font 請放在 fonts 裡
要 preload 的 js 請放在 js 裡

*/

(function () {
  var images = [
    // 'static/images/img-sv-01.png',
  ];

  var fonts = [
  	// {fontFamily: 'impact', url: 'url(static/fonts/impact.ttf)'},
	];

  var js = [
    // 'static/libs/jquery-3.6.0.min.js',
  ];

  var imagesCall = images.map((img) => {
    return new Promise ((resolve) => {
      var image = new Image();

      image.onload = resolve;
      image.src = img;
    });
  });

  var fontsCall = fonts.map((font) => {
    return new Promise((resolve) => {
      var ff = new FontFace(font.fontFamily, font.url);
      ff.load().then(resolve);
    })
  });

  var jsCall = js.map((js) => new Promise((resolve) => {
    const script = document.createElement('script');

    script.onload = resolve;
    script.src = js;
    document.head.appendChild(script);
  }));

  function fadeOutPageLoader() {
    const pageLoader = document.getElementById('page-loader');

    pageLoader.addEventListener('transitionend', () => pageLoader.remove());
    pageLoader.style.opacity = 0;
  }

  Promise.all([...jsCall, ...fontsCall, ...imagesCall]).then(() => {
    fadeOutPageLoader();
  });
})();