/*

控制輪播的左右按鈕 disable

*/

$(function () {
  $('.carousel').on('slide.bs.carousel', function (e) {
    const activeIndex = e.to;
    const carouselItems = [...document.querySelectorAll(`.carousel .carousel-item`)];

    if (activeIndex === carouselItems.length - 1) {
      document.getElementById('btn-next').classList.add('d-none');
    } else {
      document.getElementById('btn-next').classList.remove('d-none');
    }

    if (activeIndex === 0) {
      document.getElementById('btn-prev').classList.add('d-none');
    } else {
      document.getElementById('btn-prev').classList.remove('d-none');
    }
  });
});

