/*

回到最上面

id 是 scroll-to-top

*/

$(function () {
  window.addEventListener('scroll', function () {
    if (window.pageYOffset > 200) {
      $('#scroll-to-top').fadeIn('slow');
    } else {
      $('#scroll-to-top').fadeOut('slow');
    }
  });

  document.getElementById('scroll-to-top').addEventListener('click', function () {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });

    return false;
  });
});