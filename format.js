/*

1. format export 的 html 們並統一放在 build/ 底下
2. 把 static/ 下的東西也複製到 build/
3. copy css

*/

const pretty = require('pretty');
const fs = require('file-system');
const regHtml = /index.html/;

fs.rmdirSync('build');

const format = (data) => {
  const reg = /(<link[\s]rel="preload"(.*)\/>)|(<link[\s]rel="stylesheet"[\s]href="\/_next\/(.*\/)>)|(<div[\s]id="__next-error"><\/div>)|(<script(.*)src="\/_next\/(.*)"><\/script>)|(<script[\s]id="__NEXT_DATA__"(\w|\W)*>(\w|\W)*<\/script>)|(\sid="__next")|(<meta[\s]name="next-head-count"[\s]content="[\d]*"[\s]\/>)|(<noscript[\s]data-n-css=""><\/noscript>)|/g;
  const regHref = data.replace(/<a(.*?)href="(.*?)"(.*?)>(.*?)<\/a>/g, (match, p1, p2, p3, p4) => {
    if (p2.indexOf('#') > -1 || p2.indexOf('http') > -1) {
      return `<a${p1}href="${p2}"${p3}>${p4}</a>`;
    }
    return `<a${p1}href="${p2}.html"${p3}>${p4}</a>`;
  });

  let prettier = pretty(regHref, { ocd: false });
  prettier = prettier.replace(reg, '');

  return prettier;
}

// format html
fs.recurse('out', ['!404.html', '*.html'], (filepath, relative, filename) => {
  fs.readFile(filepath, 'utf8', (err,data) => {
    fs.writeFile(`build/${filename}`, format(data), (err) => {
      if (err) return console.log(err);
      console.log(`DONE!! ${filename}`);
    });
  });
});

// copy static files
fs.copySync('static', 'build/static/');

// copy css
// fs.readdir('out/_next/static/css/', (err, files) => {
//   if (err) return console.log(err);
//   fs.copyFile(`out/_next/static/css/${files[0]}`, 'build/static/styles/bootstrap.css', () => {
//     if (err) return console.log(err);
//     console.log(`DONE!! bootstrap.css`);
//   });
// });
