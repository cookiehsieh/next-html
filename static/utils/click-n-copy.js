/*

按了按鈕就複製文字

按鈕本人要有：
1. 固定的 id #btn-share
2. data-url="要複製的文字"

範例：
<button id="btn-share" data-url="https://streetvoice.com">Share</button>
*/

$(function () {
  $('body').on('click', '#btn-share', function () {
    const value = this.dataset.url;
    const tempInput = document.createElement('input');

    tempInput.style = 'position: absolute; left: -1000px; top: -1000px';
    tempInput.value = value;

    document.body.appendChild(tempInput);
    tempInput.select();
    document.execCommand('copy');
    window.getSelection().removeAllRanges();
    document.body.removeChild(tempInput);
  });
});