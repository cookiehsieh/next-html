const withPlugins = require('next-compose-plugins');
const withSass = require('@zeit/next-sass');

const webpackConfiguration = {
  webpack: (config) => {
    config.module.rules.push({
      test: /\.jsx?$/,
      exclude: /node_modules/,
      use: 'class-to-classname',
    });
    config.module.rules.push({
      test: /\.jsx?$/,
      exclude: /node_modules/,
      use: './dom-property.js',
    });
    config.module.rules.push({
      test: /\.*$/,
      exclude: /node_modules/,
      include: /static\/images/,
      use: [
        {
          loader: 'file-loader',
          options: {
            publicPath: process.env.NODE_ENV === 'production' ? "../images/" : '/static/images/',
            esModule: false,
            name: '[name].[ext]'
          }
        }
      ],
    }, {
      test: /\.*/,
      exclude: /node_modules/,
      include: /static\/fonts/,
      use: [
        {
          loader: 'file-loader',
          options: {
            publicPath: process.env.NODE_ENV === 'production' ? "../fonts/" : '/static/fonts/',
            esModule: false,
            name: '[name].[ext]'
          }
        }
      ],
    });

    return config;
  },
};

module.exports = withPlugins([withSass], webpackConfiguration);
