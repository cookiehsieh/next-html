/*

modal-toast 淡出

預設為長出 2 秒後淡出

*/

$(function () {
  $('body').on('shown.bs.modal', '.modal-toast', function () {
    setTimeout(function () {
      $('.modal-toast').modal('hide');
    }, 2000);
  });
});