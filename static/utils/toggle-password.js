/*

密碼的眼睛

*/

$(function () {
  $('body').on('click', '.toggle-password', function () {
    var input = document.querySelector(this.getAttribute('toggle'));

    this.classList.toggle('icon-shielding');
    input.type = input.type === 'password' ? 'text' : 'password';
  });
});