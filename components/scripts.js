import { Fragment } from 'react';

export default () => (
  <Fragment>
    <script src="static/js/init.js" />
  </Fragment>
);
