import Layout from 'components/layout';
import Footer from 'components/footer';
import Link from 'next/link';

export default () => (
  <Layout title="StreetVoice">
    <div class="test">HELLO WORLD</div>
    <a href="test"><span class="icon-arrow-down" style={{ color: 'red' }}>click</span></a>
    <Footer />
  </Layout>
);
