import Head from 'next/head';
import { Fragment } from 'react';
import Scripts from 'components/scripts';
import BottomScripts from 'components/bottom-scripts';

import "static/scss/bootstrap.scss";

export default ({children, title = 'StreetVoice', bottomScripts = []}) => (
  <Fragment>
    <Head>
    	<meta charSet="utf-8" />
    	<title>{ title }</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    	<meta name="apple-itunes-app" content="app-id=804704919" />
      <link rel="stylesheet" href={process.env.NODE_ENV === 'production' ?
        "static/styles/bootstrap.css" :
        "/_next/static/css/styles.chunk.css"} />
    </Head>
    { children }
    <Scripts />
    <BottomScripts bottomScripts={bottomScripts} />
  </Fragment>
);
